import React, { useState, useEffect } from 'react';
import './App.css';

import DonutLargeIcon from '@material-ui/icons/DonutLarge';
import ChatIcon from '@material-ui/icons/Chat';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import SearchIcon from '@material-ui/icons/Search';

import ChatListItem from './components/ChatListItem';
import ChatIntro from './components/ChatIntro';
import ChatWindow from './components/ChatWindow';
import NewChat from './components/NewChat';

function App() {

	const [chatList, setChatList] = useState([
		{
			charId: 1, 
			title: 'Fulano', 
			image: 'https://www.w3schools.com/howto/img_avatar2.png'
		},
		{
			charId: 2, 
			title: 'Ciclano', 
			image: 'https://www.w3schools.com/howto/img_avatar2.png'
		},
		{
			charId: 3, 
			title: 'Beltrano', 
			image: 'https://www.w3schools.com/howto/img_avatar2.png'
		}
	]);
	const [activeChat, setActiveChat] = useState({});
	const [user, setUser] = useState({
		id: 1,
		avatar: 'https://www.w3schools.com/howto/img_avatar2.png',
		name: 'Julio Cesar'
	});

	const [showNewChat, setShowNewChat] = useState(false);

	const handleNewChat = () => {
		setShowNewChat(true);
	}

	return (
		<div className="app-window">
			<div className="sidebar">
				<NewChat 
					chatList={ chatList }
					user={ user }
					show={ showNewChat }
					setShow={ setShowNewChat }
				/>
				<header>
					<img className="header--avatar" src={ user.avatar } alt="" />
					<div className="header--buttons">
						<div className="header--btn" >
							<DonutLargeIcon style={{color: '#919191'}} />
						</div>

						<div 
							onClick={ handleNewChat }
							className="header--btn" 
						>
							<ChatIcon style={{color: '#919191'}} />
						</div>

						<div className="header--btn" >
							<MoreVertIcon style={{color: '#919191'}} />
						</div>
					</div>
				</header>

				<div className="search">
					<div className="search--input">
						<SearchIcon fontSize="small"  style={{color: '#919191'}}/>
						<input type="search" placeholder="Procurar ou começar nova converva" />
					</div>
				</div>

				<div className="chatlist">
					{chatList.map((item, index) => (
						<ChatListItem 
							key={index}
							data={item}
							active={activeChat.charId === chatList[index].charId}
							click={() => setActiveChat(chatList[index])}
						/>
					))}
				</div>
			</div>

			<div className="contentarea">
				{activeChat.charId !== undefined && 
					<ChatWindow 
						user={ user }
					/>
				}

				{activeChat.charId == undefined && 
					<ChatIntro />
				}
				
			</div>
		</div>
	);
}

export default App;
