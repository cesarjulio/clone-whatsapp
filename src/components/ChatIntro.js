import React from 'react';
import './ChatIntro.css';
import intro from './intro-connection.jpg';

export default () => {
    return (
        <div className="chatIntro">
            <img src={intro} alt="" />
            <h1>Mantenha seu celular conectado</h1>
            <h2>
                O WhatsApp conecta ao seu telefone para sincronizar suas mensagens. <br/>
                Para reduzir o uso de dados, conecte seu teleone a uma rede Wi-Fi.
            </h2>
        </div>
    );
}